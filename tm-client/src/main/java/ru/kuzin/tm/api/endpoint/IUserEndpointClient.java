package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.*;
import ru.kuzin.tm.dto.response.*;

public interface IUserEndpointClient {

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserViewProfileResponse showUserProfile(@NotNull final UserViewProfileRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

}