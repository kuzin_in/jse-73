package ru.kuzin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void save(@Nullable final String userId, @Nullable final Project project);

    void saveAll(@Nullable final String userId, @Nullable final Collection<Project> projects);

    void removeAll(@Nullable final String userId);

    void removeAll(@Nullable final String userId, @Nullable Collection<Project> projects);

    void removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeOne(@Nullable final String userId, @Nullable final Project project);

    @NotNull
    List<Project> findAll(@Nullable final String userId);

    @Nullable
    Project findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    String getProjectNameById(@Nullable final String userId, @Nullable final String id);

}