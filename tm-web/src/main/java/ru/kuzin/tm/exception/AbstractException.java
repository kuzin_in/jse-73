package ru.kuzin.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull String message) {
        super(message);
    }

}
