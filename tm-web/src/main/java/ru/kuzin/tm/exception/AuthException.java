package ru.kuzin.tm.exception;

public final class AuthException extends AbstractException {

    public AuthException() {
        super("Error! Credentials are wrong...");
    }

}